const build = require('buildscript-utils')
const glue = require('json-glue')
const pathm = require('path')

const tasks = {
    watch() {
        build.spawn(`./node_modules/.bin/tsc -w -p ${__dirname}/src/server/tsconfig.json`)
        build.spawn(`./node_modules/.bin/webpack --watch`, undefined, undefined, true)
        build.spawn(`./node_modules/.bin/tsc -w -p ${__dirname}/src/client/tsconfig-noemit.json`)
    },

    async schemas() {
        await glue.generate({
            srcFolder : pathm.resolve(__dirname, 'src', 'server', 'schemas'),
            destFolder : pathm.resolve(__dirname, 'src', 'server', 'generated')
        })
    }
}
build.runTask(tasks)