"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const sharp = require("sharp");
const pathm = require("path");
const fs = require("fs");
const promisify_1 = require("promisify");
const projectRoot = pathm.resolve(__dirname, '..', '..');
const resourceRoot = pathm.resolve(projectRoot, 'resources');
const spriteSrc = pathm.resolve(resourceRoot, 'sprites_src');
const spriteDest = pathm.resolve(resourceRoot, 'temp');
var DIRECTIONS;
(function (DIRECTIONS) {
    DIRECTIONS[DIRECTIONS["DOWN"] = 0] = "DOWN";
    DIRECTIONS[DIRECTIONS["LEFT"] = 1] = "LEFT";
    DIRECTIONS[DIRECTIONS["RIGHT"] = 2] = "RIGHT";
    DIRECTIONS[DIRECTIONS["TOP"] = 3] = "TOP";
})(DIRECTIONS || (DIRECTIONS = {}));
function init() {
    return __awaiter(this, void 0, void 0, function* () {
        const transformer = ({ left, top, width, height }) => {
            return sharp()
                .extract({ left, top, width, height })
                .toFormat('png')
                .on('error', err => console.log(err));
        };
        const fromImage = (path) => __awaiter(this, void 0, void 0, function* () {
            try {
                let { width, height } = yield sharp(path).metadata();
                let { name, base, ext, dir } = pathm.parse(path);
                let fileDir = pathm.resolve(spriteDest, name);
                try {
                    yield promisify_1.promisify(fs.stat, fileDir);
                }
                catch (err) {
                    yield promisify_1.promisify(fs.mkdir, fileDir);
                }
                let pixiJson = {
                    frames: {},
                    meta: {
                        app: "script by will",
                        version: '1.0',
                        image: base,
                        size: { w: width, h: height },
                        scale: 1
                    }
                };
                let wait = [0, 1, 2, 3].map(direction => {
                    let wait = [0, 1, 2].map(instance => {
                        return new Promise((res) => __awaiter(this, void 0, void 0, function* () {
                            let fileRead = fs.createReadStream(path);
                            let filename = DIRECTIONS[direction] + '_' + instance + ext;
                            let coords = {
                                left: width / 3 * instance,
                                top: height / 4 * direction,
                                width: width / 3,
                                height: height / 4
                            };
                            pixiJson.frames[filename] = {
                                frame: { x: coords.left, y: coords.top, w: coords.width, h: coords.height },
                                rotated: false,
                                trimmed: false,
                                spriteSourceSize: { x: 0, y: 0, w: coords.width, h: coords.height },
                                sourceSize: { w: 32, h: 32 }
                            };
                            fileRead
                                .pipe(transformer(coords))
                                .pipe(fs.createWriteStream(pathm.resolve(fileDir, filename)))
                                .on('close', res);
                        }));
                    });
                    return Promise.all(wait);
                });
                yield Promise.all(wait);
                yield promisify_1.promisify(fs.writeFile, pathm.resolve(dir, name + '.json'), JSON.stringify(pixiJson, null, 2));
            }
            catch (err) {
                console.error(`Error on image ${path}`);
                throw err;
            }
        });
        let [paths] = yield promisify_1.promisify(fs.readdir, pathm.resolve(spriteSrc));
        yield Promise.all(paths.map((path) => __awaiter(this, void 0, void 0, function* () {
            if (!path.endsWith('png'))
                return;
            return fromImage(pathm.resolve(spriteSrc, path));
        })));
    });
}
exports.init = init;
//# sourceMappingURL=image-init.js.map