"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const express = require("express");
const ehbs = require("express-handlebars");
const path = require("path");
const sprite_atlas_1 = require("./sprite-atlas");
const sio_m = require("socket.io");
const uuidm = require("uuid");
//import imageInit = require('./image-init')
const app = express();
const http = require("http");
const httpServer = new http.Server(app);
const io = sio_m(httpServer);
const projectRoot = path.resolve(__dirname, '..', '..');
const resourceRoot = path.resolve(projectRoot, 'resources');
app.engine('.hbs', ehbs({
    extname: '.hbs',
    defaultLayout: 'layout-main',
    layoutsDir: path.resolve(resourceRoot, 'templates')
}));
app.set('view engine', '.hbs');
app.set('views', path.resolve(resourceRoot, 'templates'));
app.use('/static/sprites', express.static(path.resolve(resourceRoot, 'sprites_src')));
app.use('/static', express.static(path.resolve(projectRoot, 'built', 'client')));
app.get('/', (req, res) => {
    res.render('empty', { pageName: 'dummy' });
});
app.get('/remote', (req, res) => {
    res.render('remote', { pageName: 'remote' });
});
function init() {
    return __awaiter(this, void 0, void 0, function* () {
        //await imageInit.init()
        sprite_atlas_1.default();
        httpServer.listen(8090, () => console.log('rodando na 8090'));
    });
}
init();
function initGClient(uuid) {
    return {
        uuid,
        position: { x: 100, y: 100 },
        direction: { x: 0, y: 0 }
    };
}
const clients = new Map();
io.on('connection', socket => {
    console.log('received connection');
    let gclient;
    socket.on('identifier', (msg, cb) => {
        console.log('identifier');
        let uuid = msg.uuid || uuidm.v1();
        gclient = clients.get(msg.uuid);
        if (!gclient) {
            gclient = initGClient(uuid);
            clients.set(uuid, gclient);
        }
        cb({ uuid });
        socket.broadcast.emit('newplayer', gclient);
    });
    socket.on('position', (msg) => {
        console.log('position');
        if (!gclient)
            return;
        let filtered = {
            uuid: gclient.uuid,
            position: {
                x: msg.position.x,
                y: msg.position.y
            },
            direction: {
                x: msg.direction.x,
                y: msg.direction.y
            }
        };
        gclient = Object.assign({}, gclient, filtered);
        console.log('position', msg.position.x, msg.position.y);
        socket.broadcast.emit('server_position', filtered);
    });
    /*
    let shootMsgValidate = new ajv().compile({
        type : 'object',
        properties : {
            "uuid" : {
                type : 'string'
            },
            target : {
                type : 'string'
            }
        }
    })
    */
    socket.on('start_shoot', (msg) => {
        shootMsgValidate(msg);
        socket.broadcast.emit('start_shoot', msg);
    });
    socket.on('end_shoot', (msg) => {
        shootMsgValidate(msg);
        socket.broadcast.emit('end_shoot', msg);
    });
    socket.on('disconnect', () => {
        if (!gclient)
            return;
        clients.delete(gclient.uuid);
    });
});
//# sourceMappingURL=index.js.map