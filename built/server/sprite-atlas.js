"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const pathm = require("path");
const projectRoot = pathm.resolve(__dirname, '..', '..');
const resourceRoot = pathm.resolve(projectRoot, 'resources');
const spriteSrc = pathm.resolve(resourceRoot, 'sprites_src');
const fs = require("fs");
let defaultFrames = ({ rx, ry, name }) => ({
    [name + "_DOWN_0"]: {
        "frame": {
            "x": rx + 0,
            "y": ry + 0,
            "w": 32,
            "h": 32
        }
    },
    [name + "_DOWN_1"]: {
        "frame": {
            "x": rx + 32,
            "y": ry + 0,
            "w": 32,
            "h": 32
        }
    },
    [name + "_DOWN_2"]: {
        "frame": {
            "x": rx + 64,
            "y": ry + 0,
            "w": 32,
            "h": 32
        }
    },
    [name + "_LEFT_0"]: {
        "frame": {
            "x": rx + 0,
            "y": ry + 32,
            "w": 32,
            "h": 32
        }
    },
    [name + "_LEFT_1"]: {
        "frame": {
            "x": rx + 32,
            "y": ry + 32,
            "w": 32,
            "h": 32
        }
    },
    [name + "_LEFT_2"]: {
        "frame": {
            "x": rx + 64,
            "y": ry + 32,
            "w": 32,
            "h": 32
        }
    },
    [name + "_RIGHT_0"]: {
        "frame": {
            "x": rx + 0,
            "y": ry + 64,
            "w": 32,
            "h": 32
        }
    },
    [name + "_RIGHT_1"]: {
        "frame": {
            "x": rx + 32,
            "y": ry + 64,
            "w": 32,
            "h": 32
        }
    },
    [name + "_RIGHT_2"]: {
        "frame": {
            "x": rx + 64,
            "y": ry + 64,
            "w": 32,
            "h": 32
        }
    },
    [name + "_TOP_0"]: {
        "frame": {
            "x": rx + 0,
            "y": ry + 96,
            "w": 32,
            "h": 32
        }
    },
    [name + "_TOP_1"]: {
        "frame": {
            "x": rx + 32,
            "y": ry + 96,
            "w": 32,
            "h": 32
        }
    },
    [name + "_TOP_2"]: {
        "frame": {
            "x": rx + 64,
            "y": ry + 96,
            "w": 32,
            "h": 32
        }
    }
});
let defaultExtras = {
    "rotated": false,
    "trimmed": false,
    "spriteSourceSize": {
        "x": 0,
        "y": 0,
        "w": 32,
        "h": 32
    },
    "sourceSize": {
        "w": 32,
        "h": 32
    }
};
const meta = (filename, tiles_x, tiles_y) => ({
    "meta": {
        "app": "script by will",
        "version": "1.0",
        "image": filename,
        "size": {
            "w": 32 * tiles_x,
            "h": 32 * tiles_y
        },
        "scale": 1
    }
});
function generateAtlas() {
    {
        //lufia
        let frames = defaultFrames({ rx: 0, ry: 0, name: 'LUFIA' });
        Object.keys(frames).forEach(key => {
            frames[key] = Object.assign(frames[key], defaultExtras);
        });
        let out = Object.assign({ frames }, meta('lufia.png', 3, 4));
        fs.writeFileSync(pathm.resolve(spriteSrc, 'lufia.json'), JSON.stringify(out, null, 2));
    }
    {
        //redmage
        let frames = defaultFrames({ rx: 0, ry: 0, name: 'REDMAGE' });
        Object.keys(frames).forEach(key => {
            frames[key] = Object.assign(frames[key], defaultExtras);
        });
        let out = Object.assign({ frames }, meta('red_mage.png', 3, 4));
        fs.writeFileSync(pathm.resolve(spriteSrc, 'red_mage.json'), JSON.stringify(out, null, 2));
    }
    {
        //blackmage
        let frames = {};
        for (let x = 0; x < 8; x++) {
            let rx = 32 * 3 * (x % 4);
            let ry = Math.floor(x / 4) * 32 * 4;
            let base = defaultFrames({ rx, ry, name: 'BLACKMAGE' + x });
            Object.keys(base).forEach(key => {
                base[key] = Object.assign(base[key], defaultExtras);
            });
            frames = Object.assign({}, frames, base);
        }
        let out = Object.assign({ frames }, meta('blackmage.png', 3 * 4, 4 * 2));
        fs.writeFileSync(pathm.resolve(spriteSrc, 'blackmage.json'), JSON.stringify(out, null, 2));
    }
}
exports.default = generateAtlas;
//# sourceMappingURL=sprite-atlas.js.map