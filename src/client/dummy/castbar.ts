import particles = require('pixi-particles')
import px = require('pixi.js')
const emitterJson = require('./emitter.json')
import keys = require('./keys')


export function CastBar() {
    const maxWidth = 125

    const emitterContainer = new px.Container()

    const emitter = new particles.Emitter(
        emitterContainer,
        [px.Texture.fromFrame('sunfire-particle')],
        emitterJson
    )
    const initx = 200
    emitter.spawnRect.x = initx
    emitter.spawnRect.y = 450
    emitter.startSpeed = 5
    emitter.endSpeed = 5


    let out = {
        setPct(pct:number /* 0 to 1 */) {
            //emitter.spawnRect.width = maxWidth * pct
            //console.log(emitter.emit)
            emitter.spawnRect.width = Math.min(maxWidth * pct, maxWidth/6)
            if (pct > 1/6) {
                emitter.spawnRect.x = initx + maxWidth * pct - emitter.spawnRect.width
            } else {
                emitter.spawnRect.x = initx
            }
        },
        setEnabled(b:boolean) {
            emitter.emit = b
        },
        update(elapsed:number) {
            emitter.update(elapsed/1000)
        },
        emitter : () => emitter,
        sprite : () => emitterContainer
    }

    return out

}
let dummy = null as any && CastBar()
export type CastProgressT = typeof dummy


export type CastListenerOpts<T = any> = {
    bar? : CastProgressT
    maxTimeMs : number
    onStart? : () => void
    onRelease? : (pctElapsed:number /* 0 to 1 */) => void,
    context? : T
}
export function CastListener( o : CastListenerOpts ) {
    o.context = o.context || {}

    //null indicates that is not casting
    let castStartedAt : number|null = null
    let to : any

    keys.registerKeyDown(keys.KeyCodes.Q, () => {
        castStartedAt = performance.now()
        if (o.bar) {
            o.bar.setEnabled(true)
            o.bar.setPct(0) 
        }
        o.onStart && o.onStart()
        to && clearTimeout(to)
        to = setTimeout(() => {
            if (castStartedAt === null) return;
            o.onRelease && o.onRelease(1)
            castStartedAt = null
        }, o.maxTimeMs)
    })


    keys.registerKeyUp(keys.KeyCodes.Q, () => {
        if (castStartedAt === null) return
        let thisCastStart = castStartedAt
        castStartedAt = null
        let castEndedAt = performance.now()
        let pctCast = (castEndedAt - thisCastStart) / o.maxTimeMs
        o.onRelease && o.onRelease(pctCast)
    })

    return {
        update(elapsed:number) {
            if (castStartedAt !== null) {
                let pctCast = (performance.now() - castStartedAt) / o.maxTimeMs
                o.bar && o.bar.setPct(pctCast)
            } else {
                o.bar && o.bar.setEnabled(false)
            }
            o.bar && o.bar.update(elapsed)
        },
        castBar : () => o.bar
    }

}