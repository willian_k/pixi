import particles = require('pixi-particles')
import px = require('pixi.js')
const emitterJson = require('./emitter.json')


export function FirePoint(container : px.Container, extendConfig? : Partial<Config>) {

    let json2 = JSON.parse(JSON.stringify(emitterJson))
    delete json2.spawnRect
    json2 = { ...json2, 
        lifetime : { max : 0.4, min : 0.2 },
        spawnType : 'point'
    }
    json2 = Object.assign(json2, extendConfig)

    const emitter = new particles.Emitter(
        container,
        [px.Texture.fromFrame('sunfire-particle')],
        json2
    )

    return emitter

}


interface Config {
    "alpha": {
        "start": number;
        "end": number;
    };
    "scale": {
        "start": number;
        "end": number;
        "minimumScaleMultiplier": number;
    };
    "color": {
        "start": string;
        "end": string;
    };
    "speed": {
        "start": number;
        "end": number;
        "minimumSpeedMultiplier": number;
    };
    "acceleration": {
        "x": number;
        "y": number;
    };
    "maxSpeed": number;
    "startRotation": {
        "min": number;
        "max": number;
    };
    "noRotation": boolean;
    "rotationSpeed": {
        "min": number;
        "max": number;
    };
    "lifetime": {
        "min": number;
        "max": number;
    };
    "blendMode": string;
    "frequency": number;
    "emitterLifetime": number;
    "maxParticles": number;
    "pos": {
        "x": number;
        "y": number;
    };
    "addAtBack": boolean;
    "spawnType": string;
    "spawnRect": {
        "x": number;
        "y": number;
        "w": number;
        "h": number;
    };
}

