import px = require('pixi.js')
import { SelfPositionHandle, RemotePositionHandle } from './walkable'
import Updaters from './updaters'
import { Player } from './player'
import iom = require('socket.io-client')
//declare var io : SocketIOClientStatic
//const iom = io

export function init() {
    px.utils.sayHello('WebGL')

    
    const __app = new PIXI.Application(512, 512, {
        backgroundColor : 0x000000
    })

    document.body.appendChild(__app.view)
    
    px.loader
        .add('static/sprites/lufia.json')
        .add('static/sprites/red_mage.json')
        //.add('sunfire-particle', 'static/sprites/bunny.png')
        .add('sunfire-particle', 'static/sprites/sunfire-particle.png')
        .load(onLoad)

    // ----

    function onLoad() {

        const __updateList = new Updaters()
        let __socket = iom('http://localhost:8090')

        //player
        const player = Player({
            renderer : __app.renderer,
            spritePrefix : 'LUFIA',
            positionHandle : SelfPositionHandle({
                movespeed : 100,
                x : 200,
                y : 200,
                socket : __socket
            }),
            castBar : true
        })
        __app.stage.addChild(player.sprite())
        __updateList.add( elapsed => player.update(elapsed) )

        
        let other = Player({
            renderer : __app.renderer,
            spritePrefix : 'REDMAGE',
            positionHandle : RemotePositionHandle({
                movespeed : 100,
                socket : __socket
            })
        })
        __app.stage.addChild(other.sprite())
        __updateList.add( elapsed => other.update(elapsed) )
        


        //a target for the fire
        const targetPos = { x : 480, y : 250 }

        let graphics = new PIXI.Graphics()
        graphics.beginFill(0xFF3300)
        graphics.lineStyle(10, 0xffd900)
        graphics.drawCircle(targetPos.x, targetPos.y, 10)
        graphics.endFill()
        __app.stage.addChild(graphics)



        //the main loop
        __app.ticker.add(() => {
            let elapsed = __app.ticker.elapsedMS
            __updateList.update(elapsed)
        })
    }

}


/*
function skipFrameUpdater( updater : (elapsed:number) => void, nframes : number) {
    let frameCount = 0
    let accumulatedTime = 0
    return (elapsed:number) => {
        frameCount = (frameCount + 1) % nframes
        if (frameCount === 0) {
            updater(accumulatedTime)
            accumulatedTime = 0
        } else {
            accumulatedTime += elapsed
        }
    }
}
*/