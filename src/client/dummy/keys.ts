export enum Directions {
    LEFT = 65,
    UP = 87,
    RIGHT = 68,
    DOWN = 83
}


export enum KeyCodes {
    Q = 81
}

const __isKeyPressed: { [n: number]: boolean } = {}
const __keyDownHooks: { [n:number] : (() => void)[] } = {}
const __keyUpHooks: { [n: number]: (() => void)[] } = {}


export class Emitter {
    
    _fns : (() => void)[] = []

    addListener( fn : () => void ) {
        this._fns.push(fn)
    }

    trigger() {
        this._fns.forEach( fn => fn() )
    }
}
    
    
export const __emitters = {} as { [k:string] : Emitter }


listenToKey(Directions.LEFT)
listenToKey(Directions.UP)
listenToKey(Directions.RIGHT)
listenToKey(Directions.DOWN)
listenToKey(KeyCodes.Q)

export const AllDirections = [
    Directions.LEFT,
    Directions.UP,
    Directions.RIGHT,
    Directions.DOWN
]

export function anyDirectionKeyPressed() {
    return AllDirections.some( direction => __isKeyPressed[direction] )
}


function listenToKey(keycode:number) {

    __keyDownHooks[keycode] = []
    __keyUpHooks[keycode] = []

    let emitterUp = __emitters[keycode + '_UP'] = 
        __emitters[keycode + '_UP'] || new Emitter()
    window.addEventListener('keydown', event => {
        if (event.keyCode === keycode) {
            emitterUp.trigger()
            event.preventDefault()
        }
    }, false)
    emitterUp.addListener( () => {
        if (!__isKeyPressed[keycode]) {
            __isKeyPressed[keycode] = true
            __keyDownHooks[keycode].forEach(hook => hook())
        }
    })


    let emitterDown = __emitters[keycode + '_DOWN'] = 
        __emitters[keycode + '_DOWN'] || new Emitter()
    window.addEventListener('keyup', event => {
        if (event.keyCode === keycode) {
            emitterDown.trigger()
        }        
    }, false)
    __emitters[keycode + '_DOWN'].addListener( () => {
        __isKeyPressed[keycode] = false
        __keyUpHooks[keycode].forEach(hook => hook())
    })

}


export function registerKeyDown( key : KeyCodes|Directions , fn : () => void ) {
    __keyDownHooks[key].push(fn)
}


export function registerKeyUp(key: KeyCodes|Directions, fn: () => void) {
    __keyUpHooks[key].push(fn)
}


export type DirectionPair = [0|1|-1, 0|1|-1]

export function currentMoveDirection(): DirectionPair {
    if (__isKeyPressed[KeyCodes.Q])
        return [0, 0]
    return [
        __isKeyPressed[Directions.RIGHT] ? 1 :
            (__isKeyPressed[Directions.LEFT] ? -1 : 0),
        __isKeyPressed[Directions.DOWN] ? 1 :
            (__isKeyPressed[Directions.UP] ? -1 : 0)
    ]
}