import { Moveable, WalkAnimationGroup, PositionHandleT } from './walkable'
import G = require('./keys')
import px = require('pixi.js')
import { CastBar, CastListener } from './castbar'
import { FirePoint } from './firepoint'
import Updaters from './updaters'
import spline = require('cubic-spline')

const read = (str: string) => {
    let tex = px.Texture.fromFrame(str)
    tex.baseTexture.scaleMode = px.SCALE_MODES.NEAREST
    return tex
}

export function Player( o : { 
    renderer: PIXI.WebGLRenderer | PIXI.CanvasRenderer
    spritePrefix: string
    positionHandle : PositionHandleT,
    castBar? : boolean
} ) {

    const __playerContainer = new px.Container()
    const __updaterList = new Updaters()

    //groups the sprites + animation
    const animGroup = WalkAnimationGroup({
        interval: 700,
        nFrames: 3,
        sprites: {
            [G.Directions.RIGHT]: [1, 2, 0].map(n => read(`${o.spritePrefix}_RIGHT_${n}`)),
            [G.Directions.LEFT]: [1, 2, 0].map(n => read(`${o.spritePrefix}_LEFT_${n}`)),
            [G.Directions.UP]: [1, 2, 0].map(n => read(`${o.spritePrefix}_TOP_${n}`)),
            [G.Directions.DOWN]: [1, 2, 0].map(n => read(`${o.spritePrefix}_DOWN_${n}`)),
        },
        positionHandle : o.positionHandle
    })
    animGroup.sprite().scale.set(2, 2)


    //position the sprite
    const moveable = Moveable({
        sprite: animGroup.sprite(),
        positionHandle : o.positionHandle
    })
    __playerContainer.addChild(moveable.sprite())

    __updaterList.add( elapsed => {
        o.positionHandle.update(elapsed)
        animGroup.update(elapsed)
        moveable.update(elapsed)
    })

    
    //castbar test
    let firePointContainer = new px.Container()
    __playerContainer.addChild(firePointContainer)
    const targetPos = { x: 480, y: 250 }

    let castListener = CastListener({
        bar: o.castBar && CastBar() || undefined,
        maxTimeMs: 2000,
        onStart() {
            let castingFirePoint = FirePoint(firePointContainer)
            let start = animGroup.spellAnchor()
            castingFirePoint.spawnPos.set(start.x, start.y)
            this.context.castingFirePoint = castingFirePoint
            this.context.castingFirePointUpdater = (elapsed: number) => castingFirePoint.update(elapsed)
            __updaterList.add(this.context.castingFirePointUpdater)
        },
        onRelease(pct) {
            this.context.castingFirePoint.destroy()
            o.renderer.plugins.sprite.sprites.length = 0;
            __updaterList.remove(this.context.castingFirePointUpdater)

            let start = animGroup.spellAnchor()
            let end = targetPos
            let firePoint = FirePoint(firePointContainer)
            firePoint.emit = true

            let dx = end.x - start.x
            let dy = end.y - start.y
            let [mx, my] = (dx > dy) ?
                [start.x + dx * 0.5, start.y + dy * 1.2] :
                [start.x + dx * 1.2, start.y + dy * 0.5]
            let points = {
                x: [start.x, mx, end.x],
                y: [start.y, my, end.y]
            }
            const v = 100 + 200 * pct // px/s
            const angle = dy / dx
            const vn = (dx > dy) ?
                v * Math.cos(angle) :
                v * Math.sin(angle)
            let posn = 0

            let updater = (elapsed: number) => {
                firePoint.emit = true
                posn += elapsed / 1000 * vn

                if (dx > dy) {
                    let x = points.x[0] + posn
                    let y = spline(x, points.x, points.y)

                    if (x > points.x[2]) {
                        firePoint.emit = false
                        firePoint.update(elapsed / 1000)
                        setTimeout(() => {
                            __updaterList.remove(updater)
                            firePoint.destroy()
                            o.renderer.plugins.sprite.sprites.length = 0;
                        }, 200)
                        return
                    }
                    firePoint.spawnPos.x = x
                    firePoint.spawnPos.y = y
                    firePoint.update(elapsed / 1000)
                }
                else {
                    let y = points.y[0] + posn
                    let x = spline(y, points.y, points.x)

                    if (y > points.y[x]) {
                        firePoint.emit = false
                        firePoint.update(elapsed / 1000)
                        setTimeout(() => {
                            __updaterList.remove(updater)
                            firePoint.destroy()
                            o.renderer.plugins.sprite.sprites.length = 0;
                        }, 200)
                        return
                    }
                    firePoint.spawnPos.x = x
                    firePoint.spawnPos.y = y
                    firePoint.update(elapsed / 1000)
                }
            }

            __updaterList.add(updater)
        }
    })

    if (o.castBar) {
        __playerContainer.addChild(castListener.castBar()!.sprite())
    }
    __updaterList.add(
        (el: number) => castListener.update(el)   
    )

    let mvSprite = moveable.sprite()
    return {
        update(n:number) {
            __updaterList.update(n)
        },
        sprite : () => __playerContainer,
        spellAnchor : () => animGroup.spellAnchor(),
        position() {
            return {
                x : mvSprite.position.x,
                y : mvSprite.position.y
            }
        }
    }
}