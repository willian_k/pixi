type UpdaterFn = (n: number) => void
export default class Updaters {

    private fns: UpdaterFn[] = []

    add(fn: UpdaterFn) {
        this.fns.push(fn)
    }

    remove(fn: UpdaterFn) {
        let idx = this.fns.findIndex((i) => fn === i)
        if (idx >= 0) this.fns.splice(idx, 1)
    }

    update(elapsed: number) {
        this.fns.forEach(fn => fn(elapsed))
    }

}