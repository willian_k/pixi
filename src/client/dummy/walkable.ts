import px = require('pixi.js')
import G = require('./keys')
const DIRECTIONS = G.Directions


function directionPairToCode(p : G.DirectionPair) {
    let [dir0, dir1] = p
    let nextDirection: number | null = null
    if (dir0 === 1) nextDirection = DIRECTIONS.RIGHT;
    else if (dir0 === -1) nextDirection = DIRECTIONS.LEFT;
    else if (dir1 === -1) nextDirection = DIRECTIONS.UP;
    else if (dir1 === 1) nextDirection = DIRECTIONS.DOWN;
    else nextDirection = null

    return nextDirection
}


/**
 * Tracks and updates a position and direction according to the keyboard presses
 * on each tick
 */
export function SelfPositionHandle(o: { 
    x: number, 
    y: number, 
    movespeed: number
    socket :SocketIOClient.Socket
}) {
    let { x, y, socket } = o

    let _currentDirection: number | null = null
    let _justDirection: number = DIRECTIONS.RIGHT
    let uuid : string|undefined

    socket.emit('identifier', {}, (resp: any) => {
        uuid = resp.uuid
        if (resp.x) {
            ({ x, y } = resp)
        } else {
            x = 100
            y = 100
        }
    })

    let lastUpdate = performance.now()
    const netUpdate = (force = false) => () => {
        if (!uuid) return
        if (!force) {
            let sinceLast = performance.now() - lastUpdate
            if (sinceLast < 150) {
                setTimeout( netUpdate, 150 - sinceLast )
                return
            }
            lastUpdate = performance.now()
        }
        let dirs = G.currentMoveDirection()
        socket.emit('position', {
            uuid,
            position : { x, y },
            direction : {
                x : dirs[0],
                y : dirs[1]
            }
        })
    }

    G.registerKeyDown( G.Directions.DOWN, netUpdate(true) )
    G.registerKeyDown( G.Directions.UP, netUpdate(true) )
    G.registerKeyDown( G.Directions.LEFT, netUpdate(true) )
    G.registerKeyDown( G.Directions.RIGHT, netUpdate(true) )
    G.registerKeyUp( G.Directions.DOWN, netUpdate(true) )
    G.registerKeyUp( G.Directions.UP, netUpdate(true) )
    G.registerKeyUp( G.Directions.LEFT, netUpdate(true) )
    G.registerKeyUp( G.Directions.RIGHT, netUpdate(true) )    

    setInterval( netUpdate(false) , 1000 )

    let out = {
        update(elapsed: number) {

            //direction
            let [dir0, dir1] = G.currentMoveDirection()
            let nextDirection: number | null = directionPairToCode([dir0, dir1])
            _currentDirection = nextDirection
            _justDirection = _currentDirection || _justDirection

            //position
            let [mvx, mvy] = [dir0, dir1]
            let perFrame = o.movespeed * elapsed * 0.001
            mvx *= /*Math.SQRT2 */ perFrame
            mvy *= /*Math.SQRT2 */ perFrame
            x += mvx
            y += mvy
        },
        position: () => ({ x, y }),
        currentDirection: () => _currentDirection,
        justDirection: () => _justDirection
    }

    return out
}

let dummy2 = null as any && SelfPositionHandle(null as any)
export type PositionHandleT = typeof dummy2


export function RemotePositionHandle(o : { 
    socket : SocketIOClient.Socket 
    movespeed : number
}) : PositionHandleT {
    const { socket } = o

    let x : number, y : number, sx : number, sy : number
    let dirPair : G.DirectionPair = [ 0, 0 ]

    let uuid : string | undefined
    socket.emit('identifier', {}, (resp: any) => {
        uuid = resp.uuid
        if (resp.x) {
            ({x, y} = resp)
        } else {
            x = 100
            y = 100
        }
    })
    /*
    setTimeout(() => {
        if (!uuid || x === undefined) return
        socket.emit('position', {
            uuid,
            position: { x, y },
            direction: { x: dir[0], y: dir[1] }
        })
    }, 200)
    */

    let lastUpdate : number|undefined
    let _currentDirection: number | null = directionPairToCode(dirPair)
    let _justDirection: number = DIRECTIONS.RIGHT


    socket.on('server_position', (pos:any) => {
        x = sx = pos.position.x
        y = sy = pos.position.y
        dirPair[0] = pos.direction.x
        dirPair[1] = pos.direction.y
        lastUpdate = performance.now()
        _currentDirection = directionPairToCode(dirPair)
        _justDirection = _currentDirection || _justDirection
    })

    return {
        update(n:number) {
            //position
            if (!lastUpdate) return
            let elapsed = performance.now() - lastUpdate
            let [mvx, mvy] = dirPair
            let perFrame = o.movespeed * elapsed * 0.001
            //mvx *= /*Math.SQRT2 */ perFrame
            //mvy *= /*Math.SQRT2 */ perFrame
            x = sx + mvx * perFrame
            y = sy + mvy * perFrame
        },
        currentDirection: () => {
            return _currentDirection
        },
        justDirection: () => _justDirection,
        position: () => ({ x, y })
    }
}


interface MoveableOpts {
    sprite: px.Container
    positionHandle : PositionHandleT
    //onMove? : (d: G.DirectionPair, elapsed:number) => void
}

/**
 * Updates a sprite position according to a position handle
 */
export function Moveable(o: MoveableOpts) {
    let newPos = o.positionHandle.position()
    o.sprite.position.set(newPos.x, newPos.y)

    let out = {
        update(elapsed: number) {
            let newPos = o.positionHandle.position()
            o.sprite.position.set(newPos.x, newPos.y)
        },
        sprite: () => o.sprite
    }

    return out
}



interface WalkAnimationGroupOpts {
    sprites : {
        [P:number] : px.Texture[]
    }
    interval : number
    nFrames : number
    positionHandle : PositionHandleT
}

/**
 * Switches sprites and animations according to a direction
 */
export function WalkAnimationGroup( o : WalkAnimationGroupOpts ) {

    //const _eachFrame = o.interval / o.nFrames

    type SpritesT = { [x:string] : px.extras.AnimatedSprite }

    let _animIdx = G.AllDirections.reduce<SpritesT>( (out, direction) => {
        let anim = new px.extras.AnimatedSprite(o.sprites[direction])
        anim.animationSpeed = 70/o.interval
        out[direction] = anim
        return out
    } , {})

    let _animArr = Object.keys(_animIdx).map(key => _animIdx[key])

    let _group = new px.Container()
    _animArr.forEach( anim => _group.addChild(anim) )

    let _currentDirection : number|null = null

    let out = {
        update(elapsed : number) {
            let nextDirection = o.positionHandle.currentDirection()

            if (nextDirection === null) {
                _animArr.forEach( anim => {
                    anim.gotoAndStop(0)
                })
            //start moving
            } else if (_currentDirection === null) {
                _animArr.forEach( anim => {
                    anim.gotoAndPlay(0)
                })
            }

            _currentDirection = nextDirection

            G.AllDirections.forEach( direction => {
                _animIdx[direction].visible = false
            })
            _animIdx[o.positionHandle.justDirection()].visible = true
            //case 3: do nothing (keep animating)
            //console.log(_animArr[0].currentFrame)
        },

        spellAnchor() {
            let bounds = _group.getBounds()
            let y = bounds.bottom - (2/3*bounds.height)
            let x = (o.positionHandle.currentDirection() === DIRECTIONS.LEFT) ?
                bounds.left - 5 :
                bounds.right + 5
            
            return { x, y }
        },

        sprite : () => _group
    }

    return out
}