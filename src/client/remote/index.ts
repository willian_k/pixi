import iom = require('socket.io-client')
import keys = require('../dummy/keys')
import { SelfPositionHandle } from '../dummy/walkable'

export function init() {

    let socket = iom('http://172.20.10.5:8090')

    let elements = {
        [keys.Directions.UP] : document.querySelector('.up'),
        [keys.Directions.DOWN] : document.querySelector('.down'),
        [keys.Directions.LEFT] : document.querySelector('.left'),
        [keys.Directions.RIGHT] : document.querySelector('.right')
    }


    let logEl = document.getElementById('debug')!
    const log = (txt:string) => {
        logEl.innerHTML += '<br/>' + txt
    }

    Object.keys(elements).forEach(key => {
        let element = elements[key as any]!
        let isDown = false
        const down = function(ev:Event) {
            log('down, ' + ev.type)
            if (isDown) return
            isDown = true
            keys.__emitters[key + '_UP'].trigger()
        }
        const up = function(ev:Event) {
            log('up, ' + ev.type)
            if (!isDown) return
            isDown = false
            keys.__emitters[key + '_DOWN'].trigger()
        }
        element.addEventListener('touchstart', down)
        //element.addEventListener('click', down)
        element.addEventListener('touchend', up)
        element.addEventListener('mousedown', down)
        element.addEventListener('mouseup', up)
    })

    let position = SelfPositionHandle({
        movespeed : 100,
        x : 100,
        y : 100,
        socket
    })

    setInterval(() => {
        position.update(20)
    }, 20)

}

