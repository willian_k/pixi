interface GClient {
    uuid: string
    position: {
        x: number
        y: number
    }
    direction: {
        x: number
        y: number
    }
}


interface PositionMsg {
    uuid: string
    position: {
        x: number
        y: number
    }
    direction: {
        x: number
        y: number
    }
}


interface ShootMsg {
    uuid : string
    target : string
}