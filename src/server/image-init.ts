import sharp = require('sharp')
import pathm = require('path')
import fs = require('fs')
import { promisify } from 'promisify'

const projectRoot = pathm.resolve(__dirname, '..', '..')
const resourceRoot = pathm.resolve(projectRoot, 'resources')
const spriteSrc = pathm.resolve(resourceRoot, 'sprites_src')
const spriteDest = pathm.resolve(resourceRoot, 'temp')

enum DIRECTIONS {
    DOWN,
    LEFT,
    RIGHT,
    TOP
}

export async function init() {

    const transformer = ({ left, top, width, height }) => {
        return sharp()
            .extract({ left, top, width, height })
            .toFormat('png')
            .on('error', err => console.log(err))
    }


    const fromImage = async (path:string) => {
        try {
            let { width, height } = await sharp(path).metadata()
            let { name, base, ext, dir } = pathm.parse(path) 

            let fileDir = pathm.resolve(spriteDest, name)
            try {
                await promisify(fs.stat, fileDir)
            } catch(err) {
                await promisify(fs.mkdir, fileDir)
            }

            let pixiJson = {
                frames: {} as any,
                meta : {
                    app : "script by will",
                    version: '1.0',
                    image: base,
                    size : { w : width, h : height },
                    scale : 1
                }
            }

            let wait = [0, 1, 2, 3].map(direction => {
                let wait = [0, 1, 2].map(instance => {
                    return new Promise<void>(async res => {
                        let fileRead = fs.createReadStream(path)
                        let filename = DIRECTIONS[direction] + '_' + instance + ext
                        let coords = {
                            left: width! / 3 * instance,
                            top: height! / 4 * direction,
                            width: width! / 3,
                            height: height! / 4
                        }
                        pixiJson.frames[filename] = {
                            frame : { x : coords.left, y : coords.top, w : coords.width, h : coords.height },
                            rotated: false,
                            trimmed: false,
                            spriteSourceSize : { x : 0, y : 0, w : coords.width, h : coords.height },
                            sourceSize : { w : 32, h : 32 }
                        }

                        fileRead
                            .pipe( transformer(coords) )
                            .pipe( fs.createWriteStream(pathm.resolve(
                                fileDir, filename
                            )) )
                            .on('close', res)
                    })
                })
                return Promise.all(wait)
            })

            await Promise.all(wait)
            await promisify( 
                fs.writeFile,
                pathm.resolve(dir, name + '.json'), 
                JSON.stringify(pixiJson, null, 2)
            )
        } catch(err) {
            console.error(`Error on image ${path}`)
            throw err
        }
    }

    let [paths] = await promisify( fs.readdir , pathm.resolve( spriteSrc ) )
    await Promise.all(paths.map( async path => {
        if (!path.endsWith('png')) return
        return fromImage( pathm.resolve(spriteSrc, path) )
    }))

}