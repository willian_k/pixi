import express = require('express')
import ehbs = require('express-handlebars')
import path = require('path')
import makeAtlas from './sprite-atlas'
import sio_m = require('socket.io')
import uuidm = require('uuid')
//import imageInit = require('./image-init')

const app = express()
import http = require('http')
const httpServer = new http.Server(app as any)
const io = sio_m(httpServer)

const projectRoot = path.resolve(__dirname, '..', '..')
const resourceRoot = path.resolve(projectRoot, 'resources')


app.engine('.hbs', ehbs({ 
    extname: '.hbs',
    defaultLayout : 'layout-main',
    layoutsDir : path.resolve(resourceRoot, 'templates')
}))
app.set('view engine', '.hbs')
app.set('views', 
    path.resolve(resourceRoot, 'templates')
)

app.use('/static/sprites',
    express.static(path.resolve(resourceRoot, 'sprites_src'))
)

app.use('/static', 
    express.static(path.resolve(projectRoot, 'built', 'client'))
)

app.get('/', (req, res) => {
    res.render('empty', { pageName : 'dummy' })
})

app.get('/remote', (req, res) => {
    res.render('remote', { pageName : 'remote' })
})


async function init() {
    //await imageInit.init()
    makeAtlas()
    httpServer.listen(8090, () => console.log('rodando na 8090'))
}
init()




function initGClient(uuid) : Messages.GClient {
    return {
        uuid,
        position : { x : 100, y : 100 },
        direction : { x : 0, y : 0 }
    }
}

const clients = new Map<string, Messages.GClient>()

io.on('connection', socket => {

    console.log('received connection')
    let gclient : Messages.GClient|undefined

    interface IdentifierMsg {
        uuid : string
    }

    socket.on('identifier', (msg:IdentifierMsg, cb) => {
        console.log('identifier')
        let uuid = msg.uuid || uuidm.v1()
        gclient = clients.get(msg.uuid)!
        if (!gclient) {
            gclient = initGClient(uuid)
            clients.set(uuid, gclient)
        }
        cb({ uuid })
        socket.broadcast.emit('newplayer', gclient)
    })



    socket.on('position', (msg:Messages.PositionMsg) => {
        console.log('position')
        if (!gclient) return
        let filtered : Messages.PositionMsg = {
            uuid : gclient.uuid,
            position : {
                x : msg.position.x,
                y : msg.position.y
            },
            direction : {
                x : msg.direction.x,
                y : msg.direction.y
            }
        }
        gclient = { ...gclient, ...filtered }

        console.log('position', msg.position.x, msg.position.y)

        socket.broadcast.emit('server_position', filtered)
    })


    /*
    let shootMsgValidate = new ajv().compile({ 
        type : 'object',
        properties : {
            "uuid" : {
                type : 'string'
            },
            target : {
                type : 'string'
            }
        }
    })
    */
    socket.on('start_shoot', (msg:any) => {
        shootMsgValidate(msg)
        socket.broadcast.emit('start_shoot', msg)
    })
    socket.on('end_shoot', (msg:any) => {
        shootMsgValidate(msg)
        socket.broadcast.emit('end_shoot', msg)
    })

    socket.on('disconnect', () => {
        if (!gclient) return
        clients.delete(gclient.uuid)
    })
})