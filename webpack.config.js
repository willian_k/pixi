const path = require('path')
const webpack = require('webpack')
const glob = require('glob')
const _ = require('lodash')

const resolve = (...p) => path.resolve(__dirname, ...p)

module.exports = (env = {}) => {

    // { process/index : .../index.tsx }
    let mainEntries = glob.sync(resolve('src/client/*'))
        .map(path.parse)
        .filter(pathObj => !pathObj.ext) //get only folders
        .map(pathObj => pathObj.name)
        .sort((a, b) => a > b)
        .reduce((out, name) => {
            out[name + '/index'] = resolve('src/client', name, 'index.ts')
            return out
        }, {})   


    let srcMapRules = [
        {
            enforce: 'pre',
            test: /\.js$/,
            loader: "source-map-loader"
        },
        {
            enforce: 'pre',
            test: /\.ts$/,
            use: "source-map-loader"
        }
    ]

    let out = {
        entry : mainEntries,
        output : {
            filename: `[name].js`,
            path : resolve('built/client'),
            library: '[name]',
            libraryTarget: 'window'
        },

        module: {
            rules: [
                {
                    test: [/\.ts$/, /\.tsx$/],
                    loader: 'ts-loader',
                    exclude: /node_modules/,
                    options: {
                        visualStudioErrorFormat: true
                    }
                },
                ...srcMapRules,                
            ]
        },  
        
        resolve: {
            extensions: ['.tsx', '.ts', '.js']
        },

        plugins : [
            new webpack.optimize.CommonsChunkPlugin({
                name: ['commons/index', 'manifest']
            }),            
        ],

        devtool: 'inline-source-map'

    }

    return out

}